package database;

import java.io.Serializable;

public class Usuario implements Serializable {

    private int _ID;
    private String usuario;
    private String clave;
    private String nombre;

    public Usuario() {
    }

    public Usuario(int _ID, String usuario, String clave, String nombre) {
        this._ID = _ID;
        this.usuario = usuario;
        this.clave = clave;
        this.nombre = nombre;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

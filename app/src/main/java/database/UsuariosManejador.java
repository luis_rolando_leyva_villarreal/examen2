package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.zip.DeflaterInputStream;

public class UsuariosManejador {
    private Context context;
    private UsuarioDbHelper usuarioDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[]{
            DefinirTabla.Usuario._ID, DefinirTabla.Usuario.COLUMN_NAME_USUARIO, DefinirTabla.Usuario.COLUMN_NAME_CLAVE, DefinirTabla.Usuario.COLUMN_NAME_NOMBRE
    };

    public UsuariosManejador(Context context){
        this.context = context;
        this.usuarioDbHelper = new UsuarioDbHelper(this.context);
    }

    public void openDatabase(){
        db = usuarioDbHelper.getWritableDatabase();
    }

    public long insertarUsuario(Usuario usuario){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.COLUMN_NAME_USUARIO, usuario.getUsuario());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_CLAVE, usuario.getClave());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_NOMBRE, usuario.getNombre());

        return db.insert(DefinirTabla.Usuario.TABLE_NAME, null, values);
    }

    public Usuario leerUsuario(Cursor cursor){
        Usuario usuario = new Usuario();

        usuario.set_ID(cursor.getInt(0));
        usuario.setUsuario(cursor.getString(1));
        usuario.setClave(cursor.getString(2));
        usuario.setNombre(cursor.getString(3));

        return usuario;
    }

    public Usuario getUsuario(String user, String clave){
        Usuario usuario = null;

        SQLiteDatabase db = this.usuarioDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Usuario.TABLE_NAME, columnToRead, DefinirTabla.Usuario.COLUMN_NAME_USUARIO + " = ? AND " + DefinirTabla.Usuario.COLUMN_NAME_CLAVE + " = ? ", new String[] {String.valueOf(user), String.valueOf(clave)}, null, null, null);
        if (cursor.moveToFirst()){
            usuario = leerUsuario(cursor);
        }
        cursor.close();
        return usuario;
    }

    public void cerrar(){
        usuarioDbHelper.close();
    }
}

package com.example.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.Usuario;
import database.UsuariosManejador;

public class MainActivity extends AppCompatActivity {

    private EditText edtUsuario;
    private EditText edtClave;
    private Button btnIngresar;
    private Button btnRegistrar;
    private UsuariosManejador db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUsuario = (EditText) findViewById(R.id.edtUsuario);
        edtClave = (EditText) findViewById(R.id.edtClave);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        db = new UsuariosManejador(MainActivity.this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUsuario.getText().toString().equals("") || edtClave.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Por favor ingrese todos los datos para ingresar", Toast.LENGTH_SHORT).show();
                } else {
                    db.openDatabase();
                    Usuario usuario = db.getUsuario(edtUsuario.getText().toString(), edtClave.getText().toString());
                    if (usuario == null){
                        Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, IngresoActivity.class);
                        intent.putExtra("nombre", usuario.getNombre());
                        startActivityForResult(intent, 0);
                    }
                    db.cerrar();
                }
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}
